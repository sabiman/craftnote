export const environment = {
  production: true,
  develop: false,
  firebase: {
    apiKey: 'AIzaSyBIu5hHlotVQqfSJWY3OrLbDXzw0kqXrC0',
    authDomain: 'craftnote-web-challenge-3.firebaseapp.com',
    databaseURL: 'https://craftnote-web-challenge-3.firebaseio.com',
    projectId: 'craftnote-web-challenge-3',
    storageBucket: 'craftnote-web-challenge-3.appspot.com',
    messagingSenderId: '307358953668',
    appId: '1:307358953668:web:f83e7db101001626361991'
  }
};
