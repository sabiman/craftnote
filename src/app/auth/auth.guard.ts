import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AppState } from '../store/app-state.module';
import { Subscription, from, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthService } from './auth.service';
import { logoutAck, loginFiAck } from '../store/slices/user/user.actions';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private readonly router: Router,
        private readonly store: Store<AppState>,
        private readonly authService: AuthService
    ) { }

    /* Route guard checks the angular/fire current user
       it dispatches store login and logout actions to update or invalidate the store
    */
    canActivate() : Observable<boolean> {
        return from(new Promise<boolean>((res) => {
            this.authService.afAuth.authState.subscribe((user) => {
                if (user) {
                    this.store.dispatch(loginFiAck({name: user.email}));
                    res(true);
                } else {
                    this.store.dispatch(logoutAck());
                    this.router.navigate(['/login']);
                    res(false);
                }
            });
        }));
    }
}
