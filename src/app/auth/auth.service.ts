import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})

/*
  Service providing authorization and registration methods
*/
export class AuthService {

  constructor(public afAuth: AngularFireAuth) { }

  login(loginId, password) {
      return this.afAuth.auth.signInWithEmailAndPassword(loginId, password);
  }

  register(loginId, password) {
      return this.afAuth.auth.createUserWithEmailAndPassword(loginId, password);
  }

  logout() {
      return this.afAuth.auth.signOut();
  }
}
