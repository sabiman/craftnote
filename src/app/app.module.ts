import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule, NegateAuthGuard } from './app-routing.module';
import { AppComponent } from './app.component';
import { FeatureChartComponent } from './feature-chart/feature-chart.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { ChartsModule } from 'ng2-charts';
import { AppStateModule } from './store/app-state.module';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuth } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';
import { ControlsMatchDirective } from './validators/match.validator.directive';
import { PasswordValidatorDirective } from './validators/password.validator.directive';
import { NumberValidatorDirective } from './validators/number.validator.directive';
import { EmailValidatorDirective } from './validators/email.validator';




@NgModule({
    declarations: [
        AppComponent,
        FeatureChartComponent,
        RegisterComponent,
        LoginComponent,
        ControlsMatchDirective,
        PasswordValidatorDirective,
        EmailValidatorDirective,
        NumberValidatorDirective
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ChartsModule,
        AppStateModule,
        FormsModule,
        AngularFireModule.initializeApp(environment.firebase)
    ],
    providers: [
        AuthGuard,
        NegateAuthGuard,
        AngularFireAuth,
        AngularFirestore
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule { }
