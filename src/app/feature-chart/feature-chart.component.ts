import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as userActions from '../store/slices/user/user.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../store/app-state.module';
import { Observable } from 'rxjs';
import { FeatureList } from '../store/slices/features/features.interface';
import { featuresAdd } from '../store/slices/features/features.actions';
import { selectFeatures, selectUsername } from '../store/state.selectors';
import { NgForm } from '@angular/forms';

const CHART_TYPE = 'bar';
const CHART_LABEL_NAME = 'Feature Importance';
const CHART_BACKGROUND_COLOR = '#2f67a2';

@Component({
    selector: 'app-feature-chart',
    templateUrl: './feature-chart.component.html',
    styleUrls: ['./feature-chart.component.sass']
})

export class FeatureChartComponent implements OnInit {

    isAddFeatureVisible = false;
    barChartOptions: ChartOptions = {
        responsive: true,
    };
    barChartType: ChartType = CHART_TYPE;
    barChartLegend = true;
    barChartPlugins = [];

    features$: Observable<FeatureList>;
    features: FeatureList = [];
    chartDataSets: ChartDataSets[] = [{
        data: [],
        label: CHART_LABEL_NAME,
        backgroundColor: CHART_BACKGROUND_COLOR
    }];
    chartLabels: Array<string>;

    name: string;
    importance: string;
    quantity: string;

    @ViewChild('add', {read: false, static: false}) public addForm: NgForm;

    public loginId$: Observable<string>;

    constructor(
        private readonly store: Store<AppState>,
    ) { }

    // Add new feature
    onAddNew() {
        this.store.dispatch(featuresAdd({
            featureName: this.name,
            importance: parseFloat(this.importance),
            quantity: parseInt(this.quantity, 10)
            })
        );
        this.addForm.resetForm();
    }

    // Sign out user
    onSignout() {
        this.store.dispatch(userActions.logout());
        return false;
    }

    // Get all features selector to pass the data to chart
    // Data can be sorted on the fly
    ngOnInit() {
        this.features$ = this.store.select(selectFeatures);
        this.features$.subscribe(fs => {
        this.chartDataSets[0].data = [];
        this.chartLabels = [];
        // sort by featureName for the table presentation
        this.features = fs.slice(0)
                  .sort((a, b) => a.featureName.toLocaleLowerCase().localeCompare(b.featureName.toLocaleLowerCase()));
        // push features to chart arrays
        fs.forEach(feature => {
            this.chartDataSets[0].data.push(feature.importance);
            this.chartLabels.push(feature.featureName);
            });
        });

        // Get to user email selector
        this.loginId$ = this.store.select(selectUsername);
    }
}
