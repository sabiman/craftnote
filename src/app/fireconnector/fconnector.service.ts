import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentChangeAction } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { NiFeature } from '../store/slices/features/features.interface';

@Injectable({
  providedIn: 'root'
})

export class FireService {

    featuresCollection: AngularFirestoreCollection<NiFeature>;
    features$: Observable<DocumentChangeAction<NiFeature>[]>;

    constructor(private readonly afs: AngularFirestore) {
        this.featuresCollection = afs.collection<NiFeature>('features');
        this.features$ = this.featuresCollection.snapshotChanges();
    }

    getFeaturesStateChanges(): Observable<DocumentChangeAction<NiFeature>[]> {
        return this.features$;
    }

    getFeatures() {
        return this.featuresCollection.get();
    }

    addFeature(f: NiFeature) {
        this.featuresCollection.add({
            featureName : f.featureName,
            importance: f.importance,
            quantity: f.quantity
        });
    }
}
