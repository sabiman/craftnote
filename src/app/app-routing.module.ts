import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FeatureChartComponent } from './feature-chart/feature-chart.component';
import { AuthGuard } from './auth/auth.guard';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppState } from './store/app-state.module';
import { selectUsername } from './store/state.selectors';
import { Store } from '@ngrx/store';

@Injectable()
export class NegateAuthGuard implements CanActivate {    
  constructor(private _userLoggedInGuard: AuthGuard,
    private readonly store: Store<AppState>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
     return this.store.select(selectUsername).pipe(map(name => !name));
  }
}

// Routes with AuthGuard on protected route
const routes: Routes = [
  {path: 'login', component: LoginComponent },
  {path: 'register', component: RegisterComponent },
  {path: '', component: FeatureChartComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
