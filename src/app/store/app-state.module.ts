import { NgModule } from '@angular/core';
import { Action, ActionReducerMap, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { environment } from '../../environments/environment';
import { UserState } from './slices/user/user.interface';
import { FeatureEState } from './slices/features/features.interface';
import { featuresReducer } from './slices/features/features.reducer';
import { userReducer } from './slices/user/user.reducer';
import { UserEffects } from './slices/user/user.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FeaturesEffects } from './slices/features/features.effects';


export type AppReducer = (state: AppState, action: Action) => AppState;

/**
 * Global App state
 */
export interface AppState {
    user: UserState;
    features: FeatureEState;
}

// App reducers from slices
const reducers: ActionReducerMap<AppState> = {
    user: userReducer,
    features: featuresReducer
};

// Effects from slices
const effects = [
    UserEffects,
    FeaturesEffects
];

@NgModule({
    imports: [
        StoreModule.forRoot(
            reducers
      ),
      // TODO: disable dev tools in prod after released and reliable (un-comment below)
      // StoreDevtoolsModule.instrument(),
        !environment.production ? StoreDevtoolsModule.instrument({
          name: 'test app'
      }) : [],
        EffectsModule.forRoot(effects),
    ],
  })
export class AppStateModule {}
