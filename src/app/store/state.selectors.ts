import { createSelector } from '@ngrx/store';
import { AppState } from './app-state.module';
import { selectAllFeatures } from './slices/features/features.reducer';

// Select store slice
export const getFeaturesState = (state: AppState) => state.features;
export const getUserState = (state: AppState) => state.user;

// Select all Features
export const selectFeatures = createSelector(
    getFeaturesState,
    selectAllFeatures
);
// Select user email
export const selectUsername = createSelector(
    getUserState,
    (state) => state.name
);
// Select authorization status
export const selectLoggedIn = createSelector(
    getUserState,
    (state) => state.isLoggedIn
);
// Select busy state
export const selectBusy = createSelector(
    getUserState,
    (state) => state.isLoading
);
// Select error message
export const selectUserErr = createSelector(
    getUserState,
    (state) => state.errorMessage
);

