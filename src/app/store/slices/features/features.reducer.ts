import { createReducer, on } from '@ngrx/store';
import { FeatureEState, Feature } from './features.interface';
import { EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import * as featureActions from './features.actions';

// Features entity adapter
export const featuresAdapter: EntityAdapter<Feature> = createEntityAdapter<Feature>();

// Initial state from entity
export const featuresInitialState: FeatureEState = featuresAdapter.getInitialState({
    isLoading: null,
    errorMessage: null
});

// Features reducer
const pfeaturesReducer = createReducer(
    featuresInitialState,
    on(featureActions.featuresAdded, (state, data) => featuresAdapter.addOne(data, state)),

    on(featureActions.featuresAddMany, (state, data) => featuresAdapter.addMany(data.features, state)),

    on(featureActions.featuresModified, (state, data) => featuresAdapter.upsertOne(data, state)),

    on(featureActions.featuresRemoved, (state, payload) => featuresAdapter.removeOne(payload.id, state))
);

const {
    selectAll
  } = featuresAdapter.getSelectors();

export const selectAllFeatures = selectAll;

export function featuresReducer(state: FeatureEState | undefined, action: featureActions.ALL) {
    return pfeaturesReducer(state, action);
}
