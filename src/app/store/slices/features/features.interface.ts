import { EntityState } from '@ngrx/entity';

// Non indexed data type
export interface NiFeature {
    featureName: string;
    quantity: number;
    importance: number;
}

// Indexed store data
export interface Feature extends NiFeature {
    id: string;
}

// Array of indexed data
export type FeatureList = Array<Feature>;

// Payload for action type
export interface FeaturesPayload {
    features: FeatureList;
}

// Generic error interface
export interface FeatErrRespPayload {
    code?: number;
    message: string;
}

// Feature's slice of store, based on entity state interface
export interface FeatureEState extends EntityState<Feature> {
  isLoading: boolean;
  errorMessage?: string;
}
