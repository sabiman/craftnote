import { createEffect, ofType, Actions, EffectNotification } from '@ngrx/effects';
import {Injectable} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app-state.module';
import { FireService } from '../../../fireconnector/fconnector.service';
import {tap, map, mergeMap, switchMap, exhaustMap, takeUntil, catchError } from 'rxjs/operators';
import { Observable, of} from 'rxjs';
import { featuresAdded, featuresModified, featuresRemoved, featuresAdd, featuresAddMany } from './features.actions';
import { loginAck, logout, loginFiAck } from '../user/user.actions';
import { Feature } from './features.interface';

// Features effects
@Injectable()
export class FeaturesEffects {

    // Maps collection state changes to store actions
    getFeatures$ = createEffect(
        () => this.fireService.getFeaturesStateChanges().pipe(
            mergeMap(actions => actions),
            map(action => {
                const data = { ...action.payload.doc.data(), id: action.payload.doc.id};
                switch (action.type) {
                    case 'added': {
                        return featuresAdded(data);
                    }
                    case 'modified':
                        return featuresModified(data);

                    case 'removed':
                        return featuresRemoved(data);
                }
            }),
            catchError((_, caught) => caught)
        )
    );

    // Add to collection effect
    add$ = createEffect(
        () => this.actions.pipe(
            ofType(featuresAdd),
            switchMap((action) => {
                this.fireService.addFeature(action);
                return of({ type: 'NOT_EXIST' });
            })
        )
    );

    // Filters features effects for unauthorized user, aslo updates the store on load
    ngrxOnRunEffects(resolvedEffects$: Observable<EffectNotification>) {
        return this.actions.pipe(
            ofType(loginAck, loginFiAck),
            tap(() => {
                // Get all collection to the store
                this.fireService.getFeatures().subscribe((data) => {
                    const features = data.docs.map((f) => {
                        return { ...f.data(), id: f.id} as Feature;
                    });
                    this.store.dispatch(featuresAddMany({features}));
                });
            }),
            // Blocks features actions after logout dispatch
            exhaustMap(() => resolvedEffects$.pipe(takeUntil(this.actions.pipe(
                ofType(logout)
            )))));
      }

    constructor(
        private readonly actions: Actions,
        private readonly store: Store<AppState>,
        private readonly fireService: FireService,
      ) { }
}
