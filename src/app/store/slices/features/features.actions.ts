import { createAction, props, union } from '@ngrx/store';
import { Feature, NiFeature, FeaturesPayload } from './features.interface';

// Features actions
export const FEATURES_REMOVED = '[Feat Fire] Feature Removed';
export const FEATURES_ADD = '[Feat Fire] Feature Add';
export const FEATURES_ADD_MANY = '[Feat ] Add Many';
export const FEATURES_ADDED = '[Feat Fire] Feature added';
export const FEATURES_MODIFIED = '[Feat Fire] Feature Modified';

export const featuresAdd = createAction(FEATURES_ADD, props<NiFeature>());
export const featuresAddMany =  createAction(FEATURES_ADD_MANY, props<FeaturesPayload>());
export const featuresAdded = createAction(FEATURES_ADDED, props<Feature>());
export const featuresModified = createAction(FEATURES_MODIFIED, props<Feature>());
export const featuresRemoved = createAction(FEATURES_REMOVED, props<Feature>());

const actions = union({
    featuresAdd,
    featuresAddMany,
    featuresAdded,
    featuresRemoved,
    featuresModified
  });

export type ALL = typeof actions;
