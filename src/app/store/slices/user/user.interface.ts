// User state interface
export interface UserState {
    name?: string;
    isLoggedIn: boolean;
    isLoading: boolean;
    errorMessage?: string;
}

// Login request interface
export interface LoginPayload {
    loginId: string;
    password: string;
}

// Login response interface
export interface LoginResponsePayload {
    name: string;
}
// Auth error response interface
export interface AuthErrRespPayload {
    code?: number;
    message: string;
}
