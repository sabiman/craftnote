import { createAction, props, union } from '@ngrx/store';
import { LoginPayload, LoginResponsePayload, AuthErrRespPayload } from './user.interface';

// User actions
export const USER_LOGIN_START = '[Login page] Login started';
export const USER_LOGIN_SUCCESS = '[Auth service] Login success';
export const USER_LOGIN_CONFIRMED = '[Auth service] Login confirmed';
export const USER_LOGIN_ERROR = '[Auth service] Login error';
export const USER_LOGOUT_START = '[Logout page] Logout started';
export const USER_LOGOUT_SUCCESS = '[Auth service] Logout success';
export const USER_LOGOUT_ERROR = '[Auth service] Logout error';
export const USER_REGISTER_START = '[Auth page] Register start';
export const USER_REGISTER_SUCCESS = '[Auth service] Register success';
export const USER_REGISTER_ERROR = '[Auth service] Register error';
export const USER_RESET_ERROR = '[Auth page] Reset error message';

export const login = createAction(USER_LOGIN_START, props<LoginPayload>());
export const loginAck = createAction(USER_LOGIN_SUCCESS, props<LoginResponsePayload>());
export const loginFiAck = createAction(USER_LOGIN_CONFIRMED, props<LoginResponsePayload>());
export const loginErr = createAction(USER_LOGIN_ERROR, props<AuthErrRespPayload>());
export const logout = createAction(USER_LOGOUT_START);
export const logoutAck = createAction(USER_LOGOUT_SUCCESS);
export const logoutErr = createAction(USER_LOGIN_ERROR, props<AuthErrRespPayload>());
export const register = createAction(USER_REGISTER_START, props<LoginPayload>());
export const registerAck = createAction(USER_REGISTER_SUCCESS, props<LoginResponsePayload>());
export const registerErr = createAction(USER_REGISTER_ERROR, props<AuthErrRespPayload>());
export const resetErr = createAction(USER_RESET_ERROR);

const actions = union({
    login,
    loginAck,
    loginErr,
    loginFiAck,
    logout,
    logoutAck,
    logoutErr,
    register,
    registerAck,
    registerErr,
    resetErr
  });

export type ALL = typeof actions;
