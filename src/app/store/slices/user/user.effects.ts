import { createEffect, ofType, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { AppState } from '../../app-state.module';
import { AuthService } from '../../../auth/auth.service';

import * as userActions from './user.actions';
import { tap, map, switchMap, switchMapTo, catchError } from 'rxjs/operators';
import { empty, Observable, of, EMPTY } from 'rxjs';

@Injectable()
export class UserEffects {

    login$ = createEffect(
        () => this.actions.pipe(
            ofType(userActions.login),
            switchMap((user) =>
                this.authService.login(user.loginId, user.password)
                    .then((resp) =>  userActions.loginAck({name: resp.user.email}))
                    .catch((err) => userActions.loginErr({message: err.message}))),
            catchError((err, caught) => {
                this.store.dispatch(userActions.loginErr({message: err.message}));

                return caught;
            })
        )
    );

    login_ack$ = createEffect(
        () => this.actions.pipe(
            ofType(userActions.loginAck),
            switchMap(() => {
                this.router.navigate(['/']);

                return EMPTY;
            })
        )
    );

    register_ack$ = createEffect(
        () => this.actions.pipe(
            ofType(userActions.registerAck),
            switchMap(() => {
                this.router.navigate(['/']);

                return EMPTY;
            })
        )
    );

    logout$ = createEffect(
        () => this.actions.pipe(
            ofType(userActions.logout),
            map((pl) => {
                this.authService.logout();
                this.router.navigate(['login']);

                return userActions.logoutAck();
            })
        )
    );

    register$ = createEffect(
        () => this.actions.pipe(
            ofType(userActions.register),
            switchMap((user) =>
                this.authService.register(user.loginId, user.password)
                    .then((resp) => userActions.registerAck({name: resp.user.email}))
                    .catch((err) => userActions.registerErr({message: err.message}))),
            catchError((err, caught) => {
                this.store.dispatch(userActions.registerErr({message: err.message}));

                return caught;
            })
        )
    );

    constructor(
        private readonly actions: Actions,
        private readonly store: Store<AppState>,
        private readonly authService: AuthService,
        private readonly router: Router,
      ) {
      }
}

