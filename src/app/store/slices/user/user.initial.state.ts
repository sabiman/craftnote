import { UserState } from './user.interface';

export const userInitialState: UserState = {
    name: null,
    isLoggedIn: false,
    isLoading: false,
    errorMessage: null
};
