import { createReducer, on } from '@ngrx/store';
import { userInitialState } from './user.initial.state';
import * as userActions from './user.actions';
import { UserState } from './user.interface';

// User reducer
const pUserReducer = createReducer(
    userInitialState,
    on(userActions.login, state =>  ({ ...state, isLoading: true, errorMessage: null, isLoggedIn: false})),

    on(userActions.loginAck, (state, resp) => ({ ...state, isLoading: false, errorMessage: null, isLoggedIn: true, name: resp.name})),

    on(userActions.loginFiAck, (state, resp) => ({ ...state, isLoading: false, errorMessage: null, isLoggedIn: true, name: resp.name})),

    on(userActions.loginErr, (state, resp) => ({ ...state, isLoading: false, errorMessage: resp.message, isLoggedIn: false, name: null})),

    on(userActions.logout, state => ({ ...state, isLoading: true, errorMessage: null, isLoggedIn: false})),

    on(userActions.logoutAck, state => ({ ...state, isLoading: false, name: null, errorMessage: null, isLoggedIn: false})),

    on(userActions.loginErr, (state, resp) => ({ ...state, isLoading: false, errorMessage: resp.message, name: null, isLoggedIn: false})),

    on(userActions.register, (state) => ({ ...state, isLoading: true, errorMessage: null, isLoggedIn: false, name: null})),

    on(userActions.registerErr, (state, resp) =>
        ({ ...state, isLoading: false, errorMessage: resp.message, isLoggedIn: false, name: null})),

    on(userActions.registerAck, (state, resp) => ({ ...state, isLoading: false, errorMessage: null, isLoggedIn: true, name: resp.name})),

    on(userActions.resetErr, (state) => ({ ...state, errorMessage: null }))
);

export function userReducer(state: UserState, action: userActions.ALL) {
    return pUserReducer(state, action);
}
