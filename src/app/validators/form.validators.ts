import { AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';

// Email validator regex source: https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address
const emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/g;
// Password complexity regex: special symbol(s), lowercase letter(s), upper case letter(s), 8 symbols long.
const passwordRegex = /(?=.+[!@#$&*]+)(?=.+[a-z]+)(?=.+[0-9]+)(?=.+[A-Z]+).{8}/g;

/* Validation function's factories */

// Email validator factory
export function validateEmailFactory(): ValidatorFn {
    return (c: AbstractControl) => {
        const notValid = !(c.value && c.value.match && c.value.match(emailRegex));

        if (notValid) {
            return {validEmail: {valid: false}};
        }

        return null;
    };
}

// Number validator factory - validates with parseFloat
export function validateNumberFactory(): ValidatorFn {
    return (c: AbstractControl) => {
        const notValid = !c.value || isNaN(parseFloat(c.value));

        if (notValid) {
            return {validNumber: {valid: false}};
        }

        return null;
    };
}

// Password validator factory
export function validatePasswordFactory(): ValidatorFn {
    return (c: AbstractControl) => {
        const notValid = !(c.value && c.value.match  && c.value.match(passwordRegex));

        if (notValid) {
            return {validPassword: {valid: false}};
        }

        return null;
    };
}

// Two form controls validator factory
// gets names of inputs and a formGroup and compares their values
export function compareValidate(a: string, b: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[a];
        const matchingControl = formGroup.controls[b];

        if (!control || !matchingControl) {
            return null;
        }

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            return null;
        }

        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    };
}
