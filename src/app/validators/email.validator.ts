import { ValidatorFn, Validator, NG_VALIDATORS, FormControl } from '@angular/forms';
import { Directive } from '@angular/core';
import { validateEmailFactory } from './form.validators';


@Directive({
    selector: '[emailValidator][ngModel]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: EmailValidatorDirective,
        multi: true }]
})

// Email validator directive
export class EmailValidatorDirective implements Validator {
    validator: ValidatorFn;

    constructor() {
        this.validator = validateEmailFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}
