import { ValidatorFn, Validator, FormControl, NG_VALIDATORS } from '@angular/forms';
import { Directive } from '@angular/core';
import { validatePasswordFactory } from '../validators/form.validators';

@Directive({
    selector: '[passValidator][ngModel]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: PasswordValidatorDirective,
        multi: true
    }]
})

// Directive for validating password
export class PasswordValidatorDirective implements Validator {
    validator: ValidatorFn;

    constructor() {
        this.validator = validatePasswordFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}
