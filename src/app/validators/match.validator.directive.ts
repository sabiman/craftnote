import { Validator, FormGroup, NG_VALIDATORS, ValidationErrors } from '@angular/forms';
import { Directive, Input } from '@angular/core';
import { compareValidate } from '../validators/form.validators';

@Directive({
    selector: '[mustMatch]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: ControlsMatchDirective,
        multi: true
    }]
})

// Directive for matching two form control's values.
export class ControlsMatchDirective implements Validator {
    @Input('mustMatch') mustMatch: string[] = [];

    validate(formGroup: FormGroup): ValidationErrors {
        return compareValidate(this.mustMatch[0], this.mustMatch[1])(formGroup);
    }
}
