import { ValidatorFn, Validator, FormControl, NG_VALIDATORS } from '@angular/forms';
import { Directive } from '@angular/core';
import { validateNumberFactory } from '../validators/form.validators';


@Directive({
    selector: '[numValidator][ngModel]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: NumberValidatorDirective,
        multi: true
    }]
})

// Directive for validating numbers
export class NumberValidatorDirective implements Validator {
    validator: ValidatorFn;

    constructor() {
        this.validator = validateNumberFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}
