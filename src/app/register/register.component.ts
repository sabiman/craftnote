import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../store/app-state.module';
import * as userActions from '../store/slices/user/user.actions';
import { Observable, Subscription } from 'rxjs';
import { selectBusy, selectUserErr } from '../store/state.selectors';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit, OnDestroy {

  @ViewChild('register', {read: false, static: false}) public regForm: NgForm;
  password: string;
  repassword: string;
  loginId: string;
  formDisabled$: Observable<boolean>;
  errMessage$: Observable<string>;
  regFormSub$: Subscription;

  constructor(private readonly router: Router,
              private readonly store: Store<AppState>
  ) { }

  // Navigate to login view
  goLogin() {
    this.router.navigate(['login']);
    return false;
  }

  // Register user
  onRegister() {
    // Dispatch register action
    this.store.dispatch(userActions.register({
      loginId: this.loginId,
      password: this.password
    }));
    // Show db errors only until form values not changed
    this.regFormSub$ = this.regForm.valueChanges.subscribe((val) => {
        this.store.dispatch(userActions.resetErr());
        this.regFormSub$.unsubscribe();
      }
    );

    return false;
  }

  // Get store selectors
  ngOnInit() {
    this.formDisabled$ = this.store.select(selectBusy);
    this.errMessage$ = this.store.select(selectUserErr);
  }

  // Release the subscription
  ngOnDestroy() {
    if (this.regFormSub$) {
       this.regFormSub$.unsubscribe();
    }
  }

}
