import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from '../store/app-state.module';
import * as userActions from '../store/slices/user/user.actions';
import { selectBusy, selectUserErr, selectUsername } from '../store/state.selectors';
import { NgForm } from '@angular/forms';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.sass']
})

export class LoginComponent implements OnInit, OnDestroy {

    @ViewChild('login', {read: false, static: false}) public regForm: NgForm;
    userName$: Observable<string>;
    errMessage$: Observable<string>;
    formDisabled$: Observable<boolean>;
    logFormSub$: Subscription;
    loginId: string;
    password: string;

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router
    ) { }

    // Navigate to register view
    goRegister() {
        this.router.navigate(['register']);
        return false;
    }

    // Navigate to dashboard, when logged in
    goHome() {
        this.router.navigate(['/']);
        return false;
    }

    onLogin() {
        // Dispatch login action with credentials
        this.store.dispatch(userActions.login({
                loginId: this.loginId,
                password: this.password
            }
        ));
        // Show db errors only until form is changed
        this.logFormSub$ = this.regForm.valueChanges.subscribe((val) => {
                this.store.dispatch(userActions.resetErr());
                this.logFormSub$.unsubscribe();
        });
        return false;
    }

    // Get store selectors
    ngOnInit() {
        this.userName$ = this.store.select(selectUsername);
        this.formDisabled$ = this.store.select(selectBusy);
        this.errMessage$ = this.store.select(selectUserErr);
        this.userName$.subscribe((name) => {
            if (name !== null) {
                this.goHome();
            }
        })
    }
    // Release the subscription
    ngOnDestroy() {
        if (this.logFormSub$) {
            this.logFormSub$.unsubscribe();
        }
    }

}
