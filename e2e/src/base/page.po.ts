import { $, $$, browser, ElementFinder, protractor, ExpectedConditions } from 'protractor';


const DEFAULT_EXPECTED_CONDITION_TIMEOUT = 100000;
/**
 * Base class for app pages. Includes methods needed by all pages.
 */
export abstract class Page {
  public selector: string;
  public url: string;

  constructor() { }

  /**
   * Check whether a page is present in the ui.
   * @return {Promise<boolean>}
   */
  public async isPresent(): Promise<boolean> {
    return await $(this.selector).isPresent();
  }

  async waitForPresenceElement (
    el: ElementFinder,
    errorMessage = `Element is not present ${el.locator().toString()}`,
    timeout: number = DEFAULT_EXPECTED_CONDITION_TIMEOUT) {
    await browser.wait(ExpectedConditions.presenceOf(el), timeout, errorMessage);
  }

}
