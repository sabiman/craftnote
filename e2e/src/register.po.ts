/* tslint:disable */
import {$, $$, browser, by, element, ElementFinder, ExpectedConditions, protractor} from 'protractor';
import { Page } from './base/page.po';

export class RegisterPage extends Page {
    public readonly url = '/register';
    public readonly formSelector = '.form-signin';
    public readonly emailSelector = '#inputRegEmail';
    public readonly passwordSelector = '#inputRegPass';
    public readonly repassSelector = '#inputRegRetypePass';
    public readonly submitSelector = 'button[type=submit]';
    public readonly emailErrSelector = 'div.form-label-group #inputRegEmail ~ .invalid-feedback div';
    public readonly passwordErrSelector = 'div.form-label-group #inputRegPass ~ .invalid-feedback div';
    public readonly repassErrSelector = 'div.form-label-group #inputRegRetypePass ~ .invalid-feedback div';
    public readonly formErrSelector = 'div.form-label-group .dummy-control ~ .invalid-feedback';
    public readonly loginLinkSelector = 'button[type=submit] ~ div a';
    public readonly form = $(this.formSelector);
    public readonly emailInput = $(this.emailSelector);
    public readonly passwordInput = $(this.passwordSelector);
    public readonly repassInput = $(this.repassSelector);
    public readonly submitButton = $(this.submitSelector);
    public readonly emailError = $(this.emailErrSelector);
    public readonly passwordError = $(this.passwordErrSelector);
    public readonly retypeError = $(this.repassErrSelector);
    public readonly loginLink = $(this.loginLinkSelector);
    public readonly formError = $(this.formErrSelector);

    async register(email: string, password: string, repass: string) {
        const EC = protractor.ExpectedConditions;

        await browser.get(this.url);
        await browser.wait(EC.presenceOf(this.emailInput));
        await this.emailInput.sendKeys(email);
        await this.passwordInput.sendKeys(password);
        await this.repassInput.sendKeys(password);
        await this.submitButton.click();
    }

    async isOpened() {
        const EC = protractor.ExpectedConditions;
        await browser.wait(EC.presenceOf(this.submitButton));
        return await this.form.getAttribute('name') === 'register';
    }

    navigateTo() {
        return browser.get(this.url) as Promise<any>;
    }

}