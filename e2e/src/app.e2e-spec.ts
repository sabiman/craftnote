import { AppPage } from './app.po';
import { LoginPage} from './login.po';
import { RegisterPage} from './register.po';
import { DashboardPage } from './dashboard.po';

import { browser, logging, By, element } from 'protractor';
import { protractor } from 'protractor/built/ptor';

const credentials = {
    email: 'test@test.test',
    password: '12AbCd3#'  
}

describe('workspace-project App', () => {
    let loginPage: LoginPage;
    let registerPage: RegisterPage;
    let dashboardPage: DashboardPage;
    let baseUrl: string;

    beforeAll(async() => {
        //browser.ignoreSynchronization = true;
        const dashboard = new DashboardPage();
      
        await dashboard.navigateTo();
        let url = await browser.getCurrentUrl();
    
        if(url === dashboard.url) {
            await dashboard.logout();
        }
    });

    describe('Exercise 1', () => {

        beforeEach(() => {
            loginPage = new LoginPage();
            registerPage = new RegisterPage();
            dashboardPage = new DashboardPage();
            baseUrl = browser.baseUrl.slice(0, -1);
        });
  

        it('should provide authorized access', async () => {
            await dashboardPage.navigateTo();
            expect(browser.getCurrentUrl()).toBe(baseUrl + loginPage.url);
        });
  
        it('should provide login view', async () => {
            await loginPage.navigateTo();
            expect(await loginPage.isOpened()).toBe(true);
        });

        it('should provide navigation to registration view', async () => {
            await loginPage.navigateTo();
            loginPage.registerLink.click();
            expect(browser.getCurrentUrl()).toBe(baseUrl + registerPage.url);
        });

        it('should provide registration view', async () => {
            await registerPage.navigateTo();
            expect(await registerPage.isOpened()).toBe(true);  
        });

        it('should provide login with valid credentials', async () => {
            await loginPage.login(credentials.email, credentials.password).then(b => console.log('login'));
            await dashboardPage.navigateTo();
            expect(browser.getCurrentUrl()).toBe(baseUrl + dashboardPage.url);
            await dashboardPage.logout();
        });

        it('should persist session after page reload', async () => {
            await loginPage.login(credentials.email, credentials.password).then(b => console.log('logged in', b));
            await browser.refresh();
            expect(browser.getCurrentUrl()).toBe(baseUrl + dashboardPage.url);
            await dashboardPage.logout();
        });

        it('should provide logout', async () => {
            await loginPage.login(credentials.email, credentials.password).then(b => console.log('logged in', b));
            await dashboardPage.logout();
            expect(await loginPage.isOpened()).toBe(true);
        });

        it('should provide email validation on register', async () => {
            const badEmailMocks = ['ttt', 'dd@', '.@.'];
        
            for(let mock in badEmailMocks) {
                await registerPage.register(mock, credentials.password, credentials.password);
                let err = await registerPage.emailError.getText();
            
                expect(err.length).toBeGreaterThan(0);       
            }
        });

        it('should provide password validation on register', async () => {
            const badPasswordMocks = ['12ABcdef', '12AB####', '123', 'ab12$###', 'aaBBaa$#'];

            for(let mock in badPasswordMocks) {
                await registerPage.register(credentials.email, mock, mock);
                let err = await registerPage.passwordError.getText();
            
                expect(err.length).toBeGreaterThan(0);       
            }

            await registerPage.register(credentials.email, badPasswordMocks[0], badPasswordMocks[0]);

            let err = await browser.isElementPresent(registerPage.retypeError);
        
            expect(err).toBe(false);
        
            await registerPage.register(credentials.email, badPasswordMocks[0], badPasswordMocks[0]);
            await registerPage.repassInput.sendKeys('bla',);
            err = await registerPage.retypeError.getText();
            
            expect(err.length).toBeGreaterThan(0);
        });


        xit('should register a new user', async () => {
            let errMsg = 'The email address is already in use by another account.';

            const EC = protractor.ExpectedConditions;

            await registerPage.register(credentials.email, credentials.password, credentials.password);
            await browser.wait(EC.presenceOf(registerPage.formError));
            let err = await registerPage.formError.getText();
        
            expect(err).toBe(errMsg);
        });

        afterEach(async () => {
        // Assert that there are no errors emitted from the browser
            const logs = await browser.manage().logs().get(logging.Type.BROWSER);
      
            expect(logs).not.toContain(jasmine.objectContaining({
                    level: logging.Level.SEVERE,
                } as logging.Entry));
        });
  
    });

    describe('Exercise 2', () => {

        beforeEach(() => {
            loginPage = new LoginPage();
            registerPage = new RegisterPage();
            dashboardPage = new DashboardPage();
  
        });

        it('should present data', async () => {
            await loginPage.login(credentials.email, credentials.password);
            await dashboardPage.isTablePresent();
      
            let rows = await dashboardPage.table.$$('tr');
      
            expect(rows.length).toBeGreaterThan(3);

        });

        it('should allow adding new data', async () => {
            let rand = Math.ceil(Math.random() * 1000000);
            let rows = await dashboardPage.table.$$('tr');

            await dashboardPage.featureName.sendKeys(`mockFN${rand}`);
            await dashboardPage.importance.sendKeys('2');
            await dashboardPage.quantity.sendKeys('3');
            await dashboardPage.submitButton.click();
            browser.sleep(2000);
            let rowsAdded = await dashboardPage.table.$$('tr');

            // TODO: this assumption would work only while no concurency in table!!!
            expect(rowsAdded.length).toBeGreaterThan(rows.length);  
        });

        it('should validate new data', async () => {

            let rows = await dashboardPage.table.$$('tr'); 
            await dashboardPage.fillAddForm('', '23', '32');
            await dashboardPage.submitData();
 
            let rowsAdded = await dashboardPage.table.$$('tr');  
          
            //TODO: this assumption would work only while no concurency in table!!!
            expect(rowsAdded.length).toBe(rows.length); 
            
            await dashboardPage.fillAddForm('bla', 'b23', '32');
            await dashboardPage.submitData();
            rowsAdded = await dashboardPage.table.$$('tr');

            //TODO: this assumption would work only while no concurency in table!!!
            expect(rowsAdded.length).toBe(rows.length); 

            await dashboardPage.fillAddForm('bla', '23', 'b32');
            await dashboardPage.submitData();
            rowsAdded = await dashboardPage.table.$$('tr');  
        
            //TODO: this assumption would work only while no concurency in table!!!
            expect(rowsAdded.length).toBe(rows.length); 
          });

          afterEach(async () => {
          // Assert that there are no errors emitted from the browser
          const logs = await browser.manage().logs().get(logging.Type.BROWSER);
          expect(logs).not.toContain(jasmine.objectContaining({
                  level: logging.Level.SEVERE,
          } as logging.Entry));
      });

    });

    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    
        expect(logs).not.toContain(jasmine.objectContaining({
                level: logging.Level.SEVERE,
            } as logging.Entry));
    });

    afterAll(async () => {
        const dashboard = new DashboardPage();
        
        await dashboard.navigateTo();
        let url = await browser.getCurrentUrl();
        
        if(url === dashboard.url) {
            await dashboard.logout();
        }
    })
});
