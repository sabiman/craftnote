/* tslint:disable */
import {$, $$, browser, by, element, protractor } from 'protractor';
import { Page } from './base/page.po';

export class LoginPage extends Page {
    public readonly url = '/login';
    public readonly formSelector = '.form-signin';
    public readonly emailSelector = '#inputEmail';
    public readonly passwordSelector = '#inputPassword';
    public readonly submitSelector = 'button[type=submit]';
    public readonly emailErrSelector = 'div.form-label-group #inputEmail ~ .invalid-feedback div';
    public readonly passwordErrSelector = 'div.form-label-group #inputPassword ~ .invalid-feedback div';
    public readonly formErrSelector = 'div.form-label-group .dummy-control ~ .invalid-feedback';
    public readonly geretElementSelector = 'nav.dash-nav';
    public readonly registerLinkSelector = 'button[type=submit] ~ div a'
    private readonly dashNavSelector = 'nav';
    public readonly greetElement = $(this.geretElementSelector);
    public readonly form = $(this.formSelector);
    public readonly dashNav = $(this.dashNavSelector);
    public readonly emailInput = $(this.emailSelector);
    public readonly passwordInput = $(this.passwordSelector);
    public readonly submitButton = $(this.submitSelector);
    public readonly registerLink = $(this.registerLinkSelector);


    async login(email: string, password: string) {
        const EC = protractor.ExpectedConditions;
        browser.ignoreSynchronization = true;
        await browser.get(this.url);
        await browser.wait(EC.presenceOf(this.emailInput));
        await this.emailInput.sendKeys(email);
        await this.passwordInput.sendKeys(password);
        await this.submitButton.click();
        await browser.sleep(5000)
        return browser.isElementPresent(element(by.css('nav')));
    }

    async isOpened() {
        const EC = protractor.ExpectedConditions;

        await browser.wait(EC.presenceOf(this.submitButton));
        return await this.form.getAttribute('name') === 'login';
    }

    seeDashBoard() {
        return browser.isElementPresent(this.greetElement);
    }

    navigateTo() {  
        return browser.get(this.url) as Promise<any>;
    }
}