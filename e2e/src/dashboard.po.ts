/* tslint:disable */
import {$, $$, browser, by, element, ElementFinder, ExpectedConditions, protractor, Key} from 'protractor';
import { Page } from './base/page.po';

export class DashboardPage extends Page {
    public url = '/';
    public readonly navSelector = 'nav.dash-nav'
    public readonly logoutSelector = 'a.sign-out';
    public readonly chartContainerSelector = 'div.chart';
    public readonly tableSelector = 'div.table-responsive table';
    public readonly featureNameSelector = 'table input[name=featureName]';
    public readonly importanceSelector = 'table input[name=importance]';
    public readonly quantitySelector = 'table input[name=quantity]';
    public readonly submitSelector = 'table button[type=submit]';
    public readonly pageNavElement = $(this.navSelector);
    public readonly featureName = $(this.featureNameSelector);
    public readonly importance = $(this.importanceSelector);
    public readonly quantity = $(this.quantitySelector);
    public readonly submitButton = $(this.submitSelector);
    public readonly logoutInput = $(this.logoutSelector);
    public readonly chartContainer = $(this.chartContainerSelector);
    public readonly table = $(this.tableSelector);

 
    async isOpened() {
        const EC = protractor.ExpectedConditions;
    
        await browser.wait(EC.presenceOf(this.pageNavElement));
        return true;
    }

    async isTablePresent() {
        const EC = protractor.ExpectedConditions; 
    
        await browser.wait(EC.presenceOf(this.table));    
    }

    async logout() {
        const EC = protractor.ExpectedConditions;

        await browser.get(this.url);
        await browser.wait(EC.presenceOf(this.logoutInput));
        await this.logoutInput.click();
    }

    async fillAddForm(name: string, importance: string, quantity: string) {
 
        await this.featureName.clear().then((a) => a);
        await this.importance.clear().then((a) => a);
        await this.quantity.clear().then((a) => a);
        await this.featureName.sendKeys(name);
        await browser.sleep(100);
        await this.importance.sendKeys(importance);
        await browser.sleep(100);
        await this.quantity.sendKeys(quantity);
        await browser.sleep(100);
    }

    async submitData() {
        await this.submitButton.click();
        await browser.sleep(3000);
    }

    navigateTo() {
        return browser.get(this.url) as Promise<any>;
    }

}